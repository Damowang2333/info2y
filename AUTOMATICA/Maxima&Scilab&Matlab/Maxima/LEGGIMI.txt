"Oss&Ragg coma - 1" e "Oss&Ragg coma - 2" utilizzano un pacchetto esterno.
Per installare il pacchetto eseguire i seguenti passi:
- Estrarre mext-master.zip (https://github.com/jlapeyre/mext)
- Aprire Maxima
- File -> Carica Pacchetto
- Selezionare e aprire il file "00mext-ibuild.lisp" in \mext-master\packages\mext_system
- Riavviare Maxima
- File -> Carica Pacchetto
- Selezionare e aprire il file "ibuild.mac" in \mext-master\packages\coma
- Riavviare Maxima

N.B: per altre funzioni del pacchetto "coma", leggere il PDF "Control_Engineering_with_Maxima.pdf"